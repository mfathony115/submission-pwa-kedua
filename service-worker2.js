const CACHE_NAME = "footbal-serviceV1";
var urlsToCache = [
  "/",
  "/manifest.json",
  "/nav.html",
  "/index.html",
  "/page/schedule.html",
  "/page/home.html",
  "/page/favteam.html",
  "/css/custom.css",
  "/css/materialize.min.css",
  "/js/materialize.min.js",
  "/js/nav.js",
  "/js/config.js",
  "/js/api.js",
  "/js/db.js",
  "/js/idb.js",
  "/images/football.png",
  "/images/epl.png",
  "/images/icon-192x192.png",
  "/images/icon-256x256.png",
  "/images/icon-512x512.png",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://fonts.gstatic.com/s/materialicons/v55/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
];

self.addEventListener("install", function (event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function (cache) {
      return cache.addAll(urlsToCache);
    })
  );
});
self.addEventListener("fetch", function (event) {
  const baseURL = 'https://api.football-data.org/v2/';
  if(event.request.url.indexOf(baseURL) > -1){
    event.respondWith(
      //cache sesuai saran menggunakan stale while validate
      caches.open(CACHE_NAME).then(function (cache){
        return cache.match(event.request).then(function (response){
          var fetchPromise = fetch(event.request).then(function (networkResponse){
            cache.put(event.request, networkResponse.clone());
            return networkResponse;
          });
          return response || fetchPromise;
        });
      })
      //caches sebelum revisi
      // caches.open(CACHE_NAME).then(function(cache){
      //   return fetch(event.request).then(function(response){
      //     cache.put(event.request.url, response.clone());
      //     return response;
      //   })
      // })
    );
  } else {
    event.respondWith(
      caches
        .match(event.request, { ignoreSearch: true  })
        .then(function (response) {
           return response || fetch (event.request);
        })
    );
  }
});
self.addEventListener("activate", function (event) {
  event.waitUntil(
    caches.keys().then(function (cahceNames) {
      return Promise.all(
        cahceNames.map(function (cacheName) {
          if (cacheName != CACHE_NAME) {
            console.log("ServiceWorker: cache" + cacheName + "dihapus");
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});
self.addEventListener('push', event => {
  var body;
  if (event.data) {
    body = event.data.text();
  } else {
    body = 'Push message no payload';
  }
  var options = {
    body: body,
    icon: 'img/notification.png',
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: 1
    }
  };
  event.waitUntil(
    self.registration.showNotification('Push Notification', options)
  );
})
